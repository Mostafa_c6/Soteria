![Soteria](https://gitlab.com/Mostafa_c6/Soteria/raw/UI/UI/soteria.png)

Soteria project has the goal of providing possibility of having and making contract with people and companies and bussinesses without having the trust to them. with this project we decide to plan completely application base solution in order to solve the problem. We were familiar with the brand new technology who is being vast in 
the world so fast and familiar with it's ability to be so adoptable with our possible costumer needs. This technology named Blockchain. Blockchain has the great power 
to provide trust relationship in our story but that's not enough. our costumer needs their money to be without going up and down it's value frequency in order to not losing in their deals. With that we felt that we need another specific concept of technology to be made specialized for any money and creating a money with static value to be trustable enough using in blockchain smartcontracts. it called Cryptocurrency.



# Blockchain





A blockchain – originally block chain – is a continuously growing list of records, called blocks, which are linked and secured using cryptography. Each block contains typically a hash pointer as a link to a previous block,[6] a timestamp and transaction data. By design, blockchains are inherently resistant to modification of the data. A blockchain is "an open, distributed ledger that can record transactions between two parties efficiently and in a verifiable and permanent way."[8] For use as a distributed ledger a blockchain is typically managed by a peer-to-peer network collectively adhering to a protocol for validating new blocks. Once recorded, the data in any given block cannot be altered retroactively without the alteration of all subsequent blocks, which needs a collusion of the network majority.





# Cryptocurrency

A cryptocurrency (or crypto currency) is a digital asset designed to work as a medium of exchange using cryptography to secure the transactions and to control the creation of additional units of the currency.[1][2] Cryptocurrencies are classified as a subset of digital currencies and are also classified as a subset of alternative currencies and virtual currencies.

Bitcoin became the first decentralized cryptocurrency in 2009.[3] Since then, numerous cryptocurrencies have been created.[4] These are frequently called altcoins, as a blend of bitcoin alternative.[5][6] Bitcoin and its derivatives use decentralized control[7] as opposed to centralized electronic money/centralized banking systems.[8] The decentralized control is related to the use of bitcoin's blockchain transaction database in the role of a distributed ledger.[9]