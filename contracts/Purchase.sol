pragma solidity ^0.4.13;
contract Purchase
{
    uint256 public value;
    address public buyer;
    address public seller;
    enum State { Created, Locked, Inactive }
    State public state;

    /// Create a new locked purchase about
    /// `msg.value / 2` Wei.
    function Purchase()
      payable
    {
      seller = msg.sender;
      value = msg.value / 2;
      require((2 * value) == msg.value);
    }

    modifier onlyBuyer()
    {
        require(msg.sender != buyer);
        _;
    }
    modifier onlySeller()
    {
        require(msg.sender != seller);
        _;
    }
    modifier inState(State _state)
    {
        require(state != _state);
        _;
    }

    event Aborted();
    event PurchaseConfirmed();
    event ItemReceived();
    event Refunded();

    /// Abort the purchase and reclaim the ether.
    /// Can only be called by the seller before
    /// the contract is locked.
    function abort()
        onlySeller
        inState(State.Created)
    {
        seller.transfer(this.balance);
        state = State.Inactive;
        Aborted();
    }
    /// Confirm the purchase as buyer.
    /// Transaction has to include `2 * value` Wei.
    /// The ether will be locked until either
    /// confirmReceived is called by the buyer
    /// or refund is called by the seller.
    function confirmPurchase()
        inState(State.Created)
        payable
    {
      require(msg.value == 2 * value);
        buyer = msg.sender;
        state = State.Locked;
        PurchaseConfirmed();
    }
    /// Confirm that you (the buyer) received the item.
    /// This will send `value` to the buyer and
    /// `3 * value` to the seller.
    function confirmReceived()
        onlyBuyer
        inState(State.Locked)
    {
        buyer.transfer(value); // We ignore the return value on purpose
        seller.transfer(this.balance); // this.balence == 3 * value
        state = State.Inactive;
        ItemReceived();
    }
    /// Fully refund the buyer. This can only be called
    /// by the seller and will send `2 * value` both to
    /// the buyer and the sender.
    function refundBuyer()
        onlySeller
        inState(State.Locked)
    {
      // todo : witdraw pattern is needed
      // this assertion MUST  pass
      require(4 * value == this.balance);

      buyer.transfer(2 * value); // We ignore the return value on purpose
      seller.transfer(this.balance);
      state = State.Inactive;
      Refunded();
    }
}
